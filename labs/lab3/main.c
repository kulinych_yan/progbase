#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <progbase.h>
#include <progbase/console.h>
void drawLine(int x0, int y0, int x1, int y1, char ch[]);
#define PI 3.1415926535
#define roundf(x) floor(x + 0.5f)
int min(int a, int b)
{
    return (a < b) ? a : b;
}

int main(void)
{
    char ch[] = " ";
    char key = 0;
    int x = 1;
    int y = 1;
    //int box
    int xbox = 1;
    int ybox = 1;
    int xwidth = 80;
    int yheight = 40;
    //line start position
    int line1x0 = 19;
    int line1y0 = 16;
    int line2x0 = 40;
    int line2y0 = 20;
    //????????????? ????? ??? ?????? ?
    int xa1 = 0;
    int ya1 = 0;
    int xa2 = 0;
    int ya2 = 0;
    int xa3 = 0;
    int ya3 = 0;
    int xa4 = 0;
    int ya4 = 0;
    //????????????? ????? ??? ?????? B
    int xb1 = 0;
    int yb1 = 0;
    int xb2 = 0;
    int yb2 = 0;
    int xb3 = 0;
    int yb3 = 0;
    int xb4 = 0;
    int yb4 = 0;

    double line1tgRad = (PI / 4);
    double line2tgRad = -(PI / 4);
    const double radStep = 0.02;

    Console_clear();
    do
    {
        Console_reset();
        Console_clear();
        //?????????? ????? ??? ?????? ?
        xa1 = -1;
        xa2 = -1;
        xa3 = -1;
        xa4 = -1;
        ya1 = -1;
        ya2 = -1;
        ya3 = -1;
        ya4 = -1;
        //
        //?????????? ????? ??? ?????? B
        xb1 = -1;
        xb2 = -1;
        xb3 = -1;
        xb4 = -1;
        yb1 = -1;
        yb2 = -1;
        yb3 = -1;
        yb4 = -1;
        //
        //?????? ?
        int a = roundf(line1y0 - tan(PI / 2 - line1tgRad) * (xwidth - line1x0));
        if (a >= 0 && a <= yheight)
        {
            ya1 = a;
            xa1 = xwidth + 1;
        }
        a = roundf(line1y0 + tan(PI / 2 - line1tgRad) * line1x0);
        if (a >= 0 && a <= yheight)
        {
            ya2 = a;
            xa2 = 1;
        }
        a = roundf(line1x0 - tan(line1tgRad) * (yheight - line1y0));
        if (a >= 0 && a <= xwidth)
        {
            ya3 = yheight;
            xa3 = a + 1;
        }
        a = roundf(line1x0 + tan(line1tgRad) * line1y0);
        if (a >= 0 && a <= xwidth)
        {
            ya4 = 0;
            xa4 = a + 1;
        }
        //?????????? ????? A
        Console_setCursorAttribute(BG_BLACK);
        Console_setCursorAttribute(FG_RED);
        Console_setCursorPosition(yheight + 4, 1);
        printf("xA = %i\nyA = %i\n", line1x0, line1y0);

        Console_setCursorAttribute(BG_BLUE);

        if (xa1 != -1 && ya1 != -1 && xa2 != -1 && ya2 != -1)
        {
            drawLine(xa1, ya1, xa2, ya2, ch);
        }
        if (xa1 != -1 && ya1 != -1 && xa3 != -1 && ya3 != -1)
        {
            drawLine(xa1, ya1, xa3, ya3, ch);
        }
        if (xa1 != -1 && ya1 != -1 && xa4 != -1 && ya4 != -1)
        {
            drawLine(xa1, ya1, xa4, ya4, ch);
        }
        if (xa2 != -1 && ya2 != -1 && xa3 != -1 && ya3 != -1)
        {
            drawLine(xa2, ya2, xa3, ya3, ch);
        }
        if (xa2 != -1 && ya2 != -1 && xa4 != -1 && ya4 != -1)
        {
            drawLine(xa2, ya2, xa4, ya4, ch);
        }
        if (xa3 != -1 && ya3 != -1 && xa4 != -1 && ya4 != -1)
        {
            drawLine(xa3, ya3, xa4, ya4, ch);
        }
        //
        //?????? B
        int b = roundf(line2y0 - tan(PI / 2 - line2tgRad) * (xwidth - line2x0));
        if (b >= 0 && b <= yheight)
        {
            yb1 = b;
            xb1 = xwidth + 1;
        }
        b = roundf(line2y0 + tan(PI / 2 - line2tgRad) * line2x0);
        if (b >= 0 && b <= yheight)
        {
            yb2 = b;
            xb2 = 1;
        }
        b = roundf(line2x0 - tan(line2tgRad) * (yheight - line2y0));
        if (b >= 0 && b <= xwidth)
        {
            yb3 = yheight;
            xb3 = b + 1;
        }
        b = roundf(line2x0 + tan(line2tgRad) * line2y0);
        if (b >= 0 && b <= xwidth)
        {
            yb4 = 0;
            xb4 = b + 1;
        }
        //?????????? ????? B
        Console_setCursorAttribute(BG_BLACK);
        Console_setCursorAttribute(FG_RED);
        Console_setCursorPosition(yheight + 7, 1);
        printf("xB = %i\nyB = %i\n", line2x0, line2y0);

        Console_setCursorAttribute(BG_BLUE);

        if (xb1 != -1 && yb1 != -1 && xb2 != -1 && yb2 != -1)
        {
            drawLine(xb1, yb1, xb2, yb2, ch);
        }
        if (xb1 != -1 && yb1 != -1 && xb3 != -1 && yb3 != -1)
        {
            drawLine(xb1, yb1, xb3, yb3, ch);
        }
        if (xb1 != -1 && yb1 != -1 && xb4 != -1 && yb4 != -1)
        {
            drawLine(xb1, yb1, xb4, yb4, ch);
        }
        if (xb2 != -1 && yb2 != -1 && xb3 != -1 && yb3 != -1)
        {
            drawLine(xb2, yb2, xb3, yb3, ch);
        }
        if (xb2 != -1 && yb2 != -1 && xb4 != -1 && yb4 != -1)
        {
            drawLine(xb2, yb2, xb4, yb4, ch);
        }
        if (xb3 != -1 && yb3 != -1 && xb4 != -1 && yb4 != -1)
        {
            drawLine(xb3, yb3, xb4, yb4, ch);
        }
        //
        //??????? ?? ?????? ab ? ????? ??????????
        double p1 = -line1y0 - tan(PI / 2 - line1tgRad) * line1x0;
        double p2 = -line2y0 - tan(PI / 2 - line2tgRad) * line2x0;
        double k1 = tan(PI / 2 - line1tgRad);
        double k2 = tan(PI / 2 - line2tgRad);
        double yp = (k1 * p2 - k2 * p1) / (k2 - k1);
        double xp = (-yp - p1) / k1;
        Console_setCursorAttribute(BG_RED);
        if (fabs(line1tgRad - line2tgRad) >= 0.18 && fabs(fabs(line1tgRad - line2tgRad) - PI) >= 0.18 && fabs(fabs(line1tgRad - line2tgRad) - 2 * PI) >= 0.18)
            drawLine(min((line1x0 + line2x0) / 2, xbox + xwidth), min((line1y0 + line2y0) / 2, ybox + yheight), min(xp + 1, xbox + xwidth), min(yp, ybox + yheight), ch);
        //?????? ?
        Console_setCursorAttribute(BG_DEFAULT);
        Console_setCursorPosition(line1y0, line1x0 + 4);
        printf("A");
        //?????? b
        Console_setCursorPosition(line2y0, line2x0 + 4);
        printf("B");
        //?????? ab
        Console_setCursorAttribute(BG_WHITE);

        drawLine(min(line1x0, xbox + xwidth), min(line1y0, ybox + yheight), min(line2x0, xbox + xwidth), min(line2y0, ybox + yheight), ch);
        //???????
        for (x = xbox; x <= xbox + xwidth; x++)
        {
            Console_setCursorAttribute(BG_MAGENTA);
            Console_setCursorPosition(ybox, x);
            printf(" ");
            Console_setCursorPosition(ybox + yheight, x);
            printf(" ");
        }
        for (y = ybox; y <= ybox + yheight; y++)
        {
            Console_setCursorPosition(y, xbox);
            printf(" ");
            Console_setCursorPosition(y, xbox + xwidth);
            printf(" ");
        }
        key = Console_getChar();
        switch (key)
        {
        //??????? ?????? ?
        case 'w':
            if (line1y0 > ybox + 1)
                line1y0 -= 1;
            break;
        case 'a':
            if (line1x0 > xbox)
                line1x0 -= 1;
            break;
        case 's':
            if (line1y0 < ybox + yheight - 1)
                line1y0 += 1;
            break;
        case 'd':
            if (line1x0 < xbox + xwidth - 2)
                line1x0 += 1;
            break;
        case 'q':
            line1tgRad -= radStep;
            if(line1tgRad < 0)
                line1tgRad += 2 * PI;
            break;
        case 'e':
            line1tgRad += radStep;
            if(line1tgRad > 2 * PI)
                line1tgRad -= 2 * PI;
            break;
        //??????? ?????? b
        case 'i':
            if (line2y0 > ybox + 1)
                line2y0 -= 1;
            break;
        case 'j':
            if (line2x0 > xbox)
                line2x0 -= 1;
            break;
        case 'k':
            if (line2y0 < ybox + yheight - 1)
                line2y0 += 1;
            break;
        case 'l':
            if (line2x0 < xbox + xwidth - 2)
                line2x0 += 1;
            break;
        case 'u':
            line2tgRad -= radStep;
            if(line2tgRad < 0)
                line2tgRad += 2 * PI;
            break;
        case 'o':
            line2tgRad += radStep;
            if(line2tgRad > 2 * PI)
            line2tgRad -= 2 *PI;
            break;
        }

    } while (key != 27);
    Console_reset();

    return 0;
}

//

void drawLine(int x0, int y0, int x1, int y1, char ch[])
{
    int dx = abs(x1 - x0);
    int dy = abs(y1 - y0);
    int sx = x1 >= x0 ? 1 : -1;
    int sy = y1 >= y0 ? 1 : -1;

    if (x0 > 0 && y0 > 0)
    {
        Console_setCursorPosition(y0, x0);
        printf("%s", ch);
    }

    if (dy <= dx)
    {
        int d = (dy << 1) - dx;
        int d1 = dy << 1;
        int d2 = (dy - dx) << 1;
        for (int x = x0 + sx, y = y0, i = 1; i <= dx; i++, x += sx)
        {
            if (d > 0)
            {
                d += d2;
                y += sy;
            }
            else
            {
                d += d1;
            }
            // draw point
            if (y > 0 && x > 0)
            {

                Console_setCursorPosition(y, x);
                printf("%s", ch);
            }
        }
    }
    else
    {
        int d = (dx << 1) - dy;
        int d1 = dx << 1;
        int d2 = (dx - dy) << 1;
        for (int y = y0 + sy, x = x0, i = 1; i <= dy; i++, y += sy)
        {
            if (d > 0)
            {
                d += d2;
                x += sx;
            }
            else
            {
                d += d1;
            }
            // draw point
            if (x > 0 && y > 0)
            {
                Console_setCursorPosition(y, x);
                printf("%s", ch);
            }
        }
    }
}
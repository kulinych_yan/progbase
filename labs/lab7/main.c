#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <progbase.h>
#include <progbase/console.h>
#include <progbase/canvas.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#define PI 3.1415926535

struct vec_2d
{
    int x;
    int y;
};

struct color fill;

struct color stroke;

struct color
{
    int red;
    int green;
    int blue;
};

struct Ball
{
    struct vec_2d loc;
    struct vec_2d vel;
    int r;
    struct color fill;
    struct color stroke;
};

int random_org(int x, int y);

bool fequals(float a, float b);

float length(struct vec_2d vec);

struct vec_2d negative(struct vec_2d vec);

struct vec_2d add(struct vec_2d a, struct vec_2d b);

struct vec_2d mult(struct vec_2d v, float n);

struct vec_2d norm(struct vec_2d v);

float distance(struct vec_2d a, struct vec_2d b);

struct vec_2d rotate(struct vec_2d vec);

float angle(struct vec_2d vec);

struct vec_2d fromPolar(float angle);

bool equals(struct vec_2d a, struct vec_2d b);

struct Ball drawBall(struct Ball drawBall);

struct Ball updateBall(struct Ball createBall, int width, int height);

struct Ball createBall(struct Ball createBall, int width, int height);

void mainTest();

//main
int main(int argc, char **argv)
{
    int nBalls = 0;

    for (int i = 0; i < argc; i++)
    {
        if (!strcmp(argv[i], "-n"))
        {
            nBalls = atoi(argv[i + 1]);
            break;
        }
        if (!strcmp(argv[i], "-t"))
        {
            mainTest();
            return 0;
        }
    }

    srand(time(0));
    struct ConsoleSize cSize = Console_size();
    const int h = cSize.rows * 2;
    const int w = cSize.columns;

    Canvas_setSize(w, h);
    Canvas_invertYOrientation();
    struct Ball balls[nBalls];
    //
    for (int i = 0; i < nBalls; i++)
    {
        balls[i] = createBall(balls[i], w, h);
    }
    //
    Console_clear();
    const long sleepMillisTime = 33;
    long t = 0;
    while (1)
    {
        // Update time
        const long dt = sleepMillisTime;
        t += dt;
        // Update parameters based on t
        for (int i = 0; i < nBalls; i++)
        {
            balls[i] = updateBall(balls[i], w, h);
        }
        // Draw
        Canvas_beginDraw();
        for (int i = 0; i < nBalls; i++)
        {
            drawBall(balls[i]);
        }
        Canvas_setColorRGB(255, 255, 255);
        for (int i = 1; i < nBalls; i++)
        {
            Canvas_strokeLine(balls[0].loc.x, balls[0].loc.y, balls[i].loc.x, balls[i].loc.y);
        }
        Canvas_endDraw();
        // end Draw
        // Pause program
        sleepMillis(sleepMillisTime);
    }

    return 0;
}
//main

int random_org(int x, int y)
{
    int r;
    r = rand() % (y - x + 1) + x;
    return r;
}
//
struct Ball createBall(struct Ball createBall, int width, int height)
{
    createBall.r = random_org(2, 10);
    createBall.loc.x = random_org(createBall.r, width - createBall.r);
    createBall.loc.y = random_org(createBall.r, height - createBall.r);
    createBall.vel.x = random_org(-4, 4);
    createBall.vel.y = random_org(-4, 4);
    createBall.fill.red = random_org(0, 255);
    createBall.fill.green = random_org(0, 255);
    createBall.fill.blue = random_org(0, 255);
    createBall.stroke.red = random_org(0, 255);
    createBall.stroke.green = random_org(0, 255);
    createBall.stroke.blue = random_org(0, 255);
    return createBall;
}
//
struct Ball updateBall(struct Ball updateBall, int width, int height)
{
    updateBall.loc.x += updateBall.vel.x;
    updateBall.loc.y += updateBall.vel.y;
    if (updateBall.loc.x < updateBall.r || updateBall.loc.x > width - updateBall.r)
    {
        updateBall.vel.x = -updateBall.vel.x;
    }
    if (updateBall.loc.y < updateBall.r || updateBall.loc.y > height - updateBall.r)
    {
        updateBall.vel.y = -updateBall.vel.y;
    }
    return updateBall;
}
//
struct Ball drawBall(struct Ball drawBall)
{
    Canvas_setColorRGB(drawBall.fill.red, drawBall.fill.green, drawBall.fill.blue);
    Canvas_fillCircle(drawBall.loc.x, drawBall.loc.y, drawBall.r);
    Canvas_setColorRGB(drawBall.stroke.red, drawBall.stroke.green, drawBall.stroke.blue);
    Canvas_strokeCircle(drawBall.loc.x, drawBall.loc.y, drawBall.r);
    return drawBall;
}
//
float length(struct vec_2d vec)
{
    float l = sqrt(vec.x * vec.x + vec.y * vec.y);
    return l;
}
//
struct vec_2d negative(struct vec_2d vec)
{
    vec.x = -vec.x;
    vec.y = -vec.y;
    return vec;
}
//
struct vec_2d add(struct vec_2d a, struct vec_2d b)
{
    struct vec_2d sum;
    sum.x = a.x + b.x;
    sum.y = a.y + b.y;
    return sum;
}
//
struct vec_2d mult(struct vec_2d vec, float n)
{
    struct vec_2d mVec = vec;
    mVec.x *= n;
    mVec.y *= n;
    return mVec;
}
//
struct vec_2d norm(struct vec_2d vec)
{
    struct vec_2d n;
    float s = sqrt(vec.x * vec.x + vec.y + vec.y);
    float inv = 1 / s;
    n.x = vec.x * inv;
    n.y = vec.y * inv;
    return n;
}
//
float distance(struct vec_2d a, struct vec_2d b)
{
    float l = sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
    return l;
}
//
struct vec_2d rotate(struct vec_2d vec)
{
    struct vec_2d newVec;
    int angle = 0;
    newVec.x = vec.x * cos(angle) - vec.y * sin(angle);
    newVec.y = vec.x * sin(angle) - vec.y * cos(angle);
    return newVec;
}
//
float angle(struct vec_2d vec)
{
    float cos;
    cos = (vec.x * (vec.x + 1) + vec.y) / (length(vec) * (vec.x + 1));
    return acos(cos) * 180 / PI;
}
//
struct vec_2d fromPolar(float angle)
{
    struct vec_2d vec;
    vec.x = cos(angle);
    vec.y = sin(angle);

    return vec;
}
//
bool equals(struct vec_2d a, struct vec_2d b)
{
    return (fequals(a.x, b.x) && fequals(a.y, b.y));
}
//
bool fequals(float a, float b)
{
    return (fabs(a - b) < 1e-3);
}
//mainTest 
void mainTest()
{
    struct vec_2d len;
    len.x = 4;
    len.y = 3;
    struct vec_2d dist;
    dist.x = 5;
    dist.y = 4;
    struct vec_2d e;
    struct vec_2d c;
    c.x = 1;
    c.y = 2;
    struct vec_2d d;
    d.x = -1;
    d.y = 0;
    struct vec_2d m;
    m.x = -4;
    m.y = -3;
    float n = 2.0;
    float k = 1.0;
    float h = n;
    float j = k;
    struct vec_2d u;
    u.x = 2;
    u.y = 4;
    struct vec_2d f;
    f.x = 1;
    f.y = 2;
    struct vec_2d i;
    i.x = 0.6;
    i.y = 0.8;
    struct vec_2d w;
    w.x = 3;
    w.y = 4;
    struct vec_2d p;
    p.x = 15;
    p.y = 20;
    struct vec_2d g;
    g.x = 2;
    g.y = -5;
    struct vec_2d r;
    r.x = 2;
    r.y = 5;
    struct vec_2d FP;
    FP.x = 1;
    FP.y = 0;
    assert(fequals(7, 7));
    assert(fequals(length(len), 5));
    assert(fequals(distance(dist, len), sqrt(2)));
    assert(equals(negative(len), m));
    assert(equals(add(f, c), u));
    assert(equals(norm(w), i));
    assert(equals(mult(w, 5), p));
    assert(equals(rotate(r), g));
    assert(equals(fromPolar(0), FP));
}
//mainTest
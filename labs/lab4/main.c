#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <progbase.h> 
#include <progbase/console.h>
#include <time.h>
#include <stdbool.h>

int getColor(char colorCode);

const char image[28][28] = {
	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
	{0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x3,0x3,0x3,0x3,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7},
	{0x7,0x7,0x7,0x7,0x7,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x7,0x7,0x7,0x7,0x7},
	{0x7,0x7,0x7,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0xF,0xF,0x3,0x3,0x3,0x3,0x3,0xF,0xF,0xF,0x3,0x3,0x3,0x3,0x3,0x7,0x7,0x7},
	{0x7,0x7,0x7,0x3,0x3,0x3,0x3,0xF,0xF,0xF,0xF,0x3,0x3,0x3,0x3,0x3,0xF,0x3,0x3,0xF,0xF,0xF,0x3,0x3,0x3,0x7,0x7,0x7},
	{0x7,0x7,0x7,0x3,0x3,0x3,0x3,0xF,0xF,0xF,0xF,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0xF,0xF,0xF,0x3,0x3,0x7,0x7,0x7},
	{0x7,0x7,0x7,0x3,0x3,0x3,0x3,0x3,0x3,0xF,0xF,0xF,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0xF,0xF,0xF,0x3,0x3,0x7,0x7,0x7},
	{0x7,0x7,0x7,0x3,0x3,0x3,0x3,0xF,0xF,0xF,0xF,0xF,0xF,0xF,0x3,0x3,0x3,0xF,0xF,0xF,0xF,0xF,0x3,0x3,0x3,0x7,0x7,0x7},
	{0x7,0x7,0x7,0x3,0x3,0x3,0xF,0x3,0x3,0xF,0xF,0xF,0xF,0xF,0xF,0xF,0xF,0xF,0xF,0x3,0x3,0x3,0x3,0x3,0x3,0x7,0x7,0x7},
	{0x7,0x7,0x7,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0xF,0xF,0xF,0xF,0xF,0xF,0xF,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x7,0x7,0x7},
	{0x7,0x7,0x7,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0xF,0xF,0xF,0xF,0xF,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x7,0x7,0x7},
	{0x7,0x7,0x7,0x3,0x3,0x3,0x3,0x3,0xF,0xF,0xF,0xF,0xF,0xF,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x7,0x7,0x7},
	{0x7,0x7,0x7,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x7,0x7,0x7},
	{0x7,0x7,0x7,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x7,0x7,0x7},
	{0x7,0x7,0x7,0x7,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x7,0x7,0x7,0x7},
	{0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x7,0x7,0x7,0x7,0x7,0x7,0x7},
	{0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7},
	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1}
};


int main(void) {

    char arraySw;
    char matrixSw;
    char key;

    bool switch1Run = true;
    bool matrixExit;
    bool arrayExit;

    float tmpArr;
    float minarr = 0;
    float maxarr = 0;
    float d = 0;
    int max_i = 0;
    int min_i = 0;
    int n = 0;
    int N = 50;

    int tmpMatrix;
    int rmax = 0;
    int rmin = 0;
    int i = 0;
    int j = 0;
    int l = 0;
    int h = 0;
    int max_x = 0;
    int max_y = 0;
    int min_x = 0;
    int min_y = 0;
    int maxMatrix = 0;
    int minMatrix = 0;

    int x1 = 0;
    int y1 = 0;
    int x2 = 0;
    int y2 = 0;

    double value = 0;
    double value1 = 0;

    float array[i];
    int matrix[i][j];

    int sum = 0;

    srand(time(0));

    while( switch1Run == true )
    {
        matrixExit = true;
        arrayExit = true;
        Console_clear();
        puts("1. Array menu");
        puts("2. Matrix menu");
        puts("3. ");
        puts("0. Exit");
        key = Console_getChar();
        switch(key)
        {
            case '1':
                printf("Enter N = ");
                scanf("%d", &n);
                for(i = 0; i < n; i ++)
                {
                    float array[i];
                    array[i] = 0;
                }
                
                Console_clear();
                puts("1. Fill an array with random numbers from L to H.");
                puts("1. Reset all elements of the array.");
                puts("3. Know the minimum array element and its index.");
                puts("4. Output the negative elements of an array.");
                puts("5. Swap the values of the maximum and minimum elements of the array.");
                puts("6. Reduce all elements of the array to the entered number.");
                puts("0. Back");
                while(arrayExit == true)
                {
                    arraySw = Console_getChar();
                    switch(arraySw)
                    {
                    case '1':
                        for(i = 0; i < n; i ++)
                        {
                            array [i] = (float)rand()/((float)RAND_MAX/N);
                            value = array [i];
                            printf("%10.3lf", value);
                        }
                        puts("");
                        break;
                    case '2':
                        for(i = 0; i < n; i ++)
                        {
                            array[i] = 0;
                            value = array [i];
                            printf("%10.3lf", value);
                        }
                        puts("");
                        break;
                    case '3':
                        min_i = 0;
                        max_i = 0;
                        minarr = array[0];
                        maxarr = array[0];
                        for( i = 0; i < n; i ++)
                        {
                            if(minarr > array[i])
                            {
                                minarr = array[i];
                                min_i = i;
                            }
                            if(maxarr < array[i])
                            {
                                maxarr = array[i];
                                max_i = i;
                            }
                        }
                        printf("min array[%d] = %5.3f\n", min_i, array[min_i]);
                        printf("max array[%d] = %5.3f\n", max_i, array[max_i]);
                        break;
                    case '4':
                        value = 0;
                        value1 = 0;
                        for(i = 0; i < n; i ++)
                        {
                            value1 = array[i];
                            if(value1 < 0)
                            {
                                if (value != 0)
                                {
                                    value = value * value1;
                                }
                                else
                                {
                                    value = value1;
                                }
                            }
                        }
                        printf("product of negative elements = %lf\n", value);
                        break;
                    case '5':
                        tmpArr = 0;
                        min_i = 0;
                        max_i = 0;
                        minarr = array[0];
                        maxarr = array[0];
                        for( i = 0; i < n; i ++)
                        {
                            if(minarr > array[i])
                            {
                                minarr = array[i];
                                min_i = i;
                            }
                            if(maxarr < array[i])
                            {
                                maxarr = array[i];
                                max_i = i;
                            }
                        }
                        tmpArr = array[min_i];
                        array[min_i] = array[max_i];
                        array[max_i] = tmpArr;
                        for(i = 0; i < n; i ++)
                        {
                            printf("%10.3f", array[i]);
                        }
                        puts("");
                        break;
                    case '6':
                        printf("Entered number = ");
                        scanf("%f", &d);
                        for(i = 0; i < n; i ++)
                        {
                            value = array[i] - d;
                            printf("%10.3lf", value);
                            array[i] = value;
                        }
                        puts("");
                        break;
                    case '0':
                        arrayExit = false;
                        break;
                    }
                }
                break;
            case '2':
            {
                printf("Enter H =  ");
                scanf("%d", &h);
                printf("Enter L =  ");
                scanf("%d", &l);
                printf("Enter min value =  ");
                scanf("%d", &rmin);
                printf("Enter max value =  ");
                scanf("%d", &rmax);
                int matrix[h][l];
                Console_clear();
                puts("1. Fill an array with random numbers from L to H.");
                puts("1. Reset all elements of the array.");
                puts("3. Find the maximum element and its indexes (i and j).");
                puts("4. Find the sum of column items by a given index.");
                puts("5. Swap places the maximum and minimum elements of the matrix.");
                puts("6. Change the value of an item by specified indexes to a specified one.");
                puts("0. Back");
                for(i = 0; i < h; i ++)
                {
                    for(j = 0; j < l; j ++)
                    {
                        int matrix[i][j];
                        matrix[i][j] = 0;
                        printf("%d ", matrix[i][j]);
                    }
                    puts("");
                }
                puts("");
                while(matrixExit == true)
                {
                    matrixSw = Console_getChar();
                    switch(matrixSw)
                    {
                    case '1':
                        for(i = 0; i < h; i ++)
                        {
                            for(j = 0; j < l; j ++)
                            {
                            matrix[i][j] = rand() % (rmax - rmin ) + rmin;
                            printf("%10d", matrix[i][j]);
                            }
                            puts("");
                        }
                        puts("");
                        break;
                    case '2':
                        for(i = 0; i < h; i ++)
                        {
                            for(j = 0; j < l; j ++)
                            {
                                matrix[i][j] = 0;
                                printf("%10d", matrix[i][j]);
                            }
                            puts("");
                        }
                        puts("");
                        break;
                    case '3':
                    {
                        min_x = 0;
                        min_y = 0;
                        max_x = 0;
                        max_y = 0;
                        maxMatrix = matrix[0][0];
                        minMatrix = matrix[0][0];
                        for(i = 0; i < h; i ++)
                        {
                            for(j = 0; j < l; j ++)
                            {
                                if(minMatrix > matrix[i][j])
                                {
                                    minMatrix = matrix[i][j];
                                    min_y = i;
                                    min_x = j;
                                }
                                if(maxMatrix < matrix[i][j])
                                {
                                    maxMatrix = matrix[i][j];
                                    max_y = i;
                                    max_x = j;
                                }
                            }
                        }
                        printf("min matrix[%d][%d] = %d\n", min_y, min_x, minMatrix);
                        printf("max matrix[%d][%d] = %d\n", max_y, max_x, maxMatrix);
                        break;
                    }
                    case '4':
                        j = 0;
                        printf("Enter column = ");
                        scanf("%d", &j);
                        for(i = 0; i < h; i ++)
                        {
                            sum += matrix[i][j];
                        }
                        printf("sum of column = %d\n", sum);
                        break;
                    case '5':
                        min_x = 0;
                        min_y = 0;
                        max_x = 0;
                        max_y = 0;
                        maxMatrix = matrix[0][0];
                        minMatrix = matrix[0][0];
                        for(i = 0; i < h; i ++)
                        {
                            for(j = 0; j < l; j ++)
                            {
                                if(minMatrix > matrix[i][j])
                                {
                                    minMatrix = matrix[i][j];
                                    min_y = i;
                                    min_x = j;
                                }
                                if(maxMatrix < matrix[i][j])
                                {
                                    maxMatrix = matrix[i][j];
                                    max_y = i;
                                    max_x = j;
                                }
                            }
                        }
                        tmpMatrix = matrix[min_y][min_x];
                        matrix[min_y][min_x] = matrix [max_y][max_x];
                        matrix[max_y][max_x] = tmpMatrix;
                        for(i = 0; i < h; i ++)
                        {
                            for(j = 0; j < l; j ++)
                            {
                                printf("%7d", matrix[i][j]);
                            }
                            puts("");
                        }
                        break;
                    case '6':
                        min_x = 0;
                        min_y = 0;
                        max_x = 0;
                        max_y = 0;
                        maxMatrix = matrix[0][0];
                        minMatrix = matrix[0][0];
                        printf("Enter x1 = ");
                        scanf("%d", &x1);
                        printf("Enter y1 = ");
                        scanf("%d", &y1);
                        printf("Enter x2 = ");
                        scanf("%d", &x2);
                        printf("Enter y2 = ");
                        scanf("%d", &y2);
                        min_x = x1;
                        min_y = y1;
                        max_x = x2;
                        max_y = y2;
                        tmpMatrix = matrix[min_y][min_x];
                        matrix[min_y][min_x] = matrix [max_y][max_x];
                        matrix[max_y][max_x] = tmpMatrix;
                        for(i = 0; i < h; i ++)
                        {
                            for(j = 0; j < l; j ++)
                            {
                                printf("%7d", matrix[i][j]);
                            }
                            puts("");
                        }
                        break;
                    case '0':
                        matrixExit = false;
                        break;
                    }
                }
                break;
            }
            case '3':
                Console_clear();
                for(i = 0; i < 28; i ++)
                {
                    for(j = 0; j < 28; j ++)
                    {
                        int color = getColor(image[i][j]);  // use inside int main() {}
                        Console_setCursorAttribute(color);
                        printf("  ");
                        Console_reset();
                    }
                    puts("");
                }
                key = Console_getChar();
                break;
            case '0':
                switch1Run = false;
                break;
            default: break;
        }
    }
    return 0;
}


int getColor(char colorCode) {
    // colors encoding table (hex code -> console color)
    const char colorsTable[16][2] = {
        {0x0, BG_BLACK},
        {0x1, BG_INTENSITY_BLACK},
        {0x2, BG_RED},
        {0x3, BG_INTENSITY_RED},
        {0x4, BG_GREEN},
        {0x5, BG_INTENSITY_GREEN},
        {0x6, BG_YELLOW},
        {0x7, BG_INTENSITY_YELLOW},
        {0x8, BG_BLUE},
        {0x9, BG_INTENSITY_BLUE},
        {0xa, BG_MAGENTA},
        {0xb, BG_INTENSITY_MAGENTA},
        {0xc, BG_CYAN},
        {0xd, BG_INTENSITY_CYAN},
        {0xe, BG_WHITE},
        {0xf, BG_INTENSITY_WHITE}
    };
    const int tableLength = sizeof(colorsTable) / sizeof(colorsTable[0]);
    for (int i = 0; i < tableLength; i++) 
    {
        char colorPairCode = colorsTable[i][0];
        char colorPairColor = colorsTable[i][1];
        if (colorCode == colorPairCode) 
        {
            return colorPairColor;  // we have found our color
        }
    }
    return 0;  // it's an error
}
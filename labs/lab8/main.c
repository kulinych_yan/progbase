#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <progbase.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
//
enum TokenType
{
    TOKEN_KEYWORD,
	TOKEN_OPERATOR,
	TOKEN_DELIMITER,
    TOKEN_IDENTIFIER,
    TOKEN_LITERAL,
};
//
enum Token_Keywords
{
    KW_INT,
    KW_LONG,
    KW_RETURN,
};
//
enum Token_Operators
{
    OP_PLUS,
    OP_MULT,
    OP_ASSIGNMENT,
};
//
enum Token_Delimiters
{
    DEL_LEFTPAR,
    DEL_RIGHTPAR,
    DEL_LEFTBRACKET,
    DEL_RIGHTBRACKET,
    DEL_COMMA,
    DEL_SEMICOLON,
};
//
enum Token_Literals
{
    LIT_INTEGER,
    LIT_FLOAT,
    LIT_STRING,
};
//
struct Table
{
    char *items;
    size_t capacity;
    size_t counter;
    size_t rowCapacity;
};
//
struct TokenList
{
    struct Token *items;
    size_t capacity;
    size_t counter;
};
//
struct Token
{
    char *lexeme;
    enum TokenType type;
    int sub_type;
};
//
struct Table createTable(char *items, int capacity, int rowCapacity);

struct TokenList createTokenList(struct Token *a, int n);

void addToken(struct TokenList *plist, struct Token newToken);

void addString(struct Table *ptable, const char *str);

char *getStringAt(const struct Table *ptable, int index);

void printToken(struct TokenList *plist);

void parseCode(char *src, struct Table *keywords, struct Table *identifiers, struct Table *literals, struct Table *operators, struct Table *delimiters, struct TokenList *plist);

char *readWord(char *str, char *dest, const int destLen);

char *readNumber(char *str, char *dest, const int destLen, int *Dot);

char *readString(char *str, char *dest, const int destLen);

bool isescape(char ch);

bool isoperator(char ch, struct Token *token);

bool isdelimiter(char ch, struct Token *token);

int main()
{
    const int nTokens = 100;
    struct Token Tokens[nTokens];
    struct TokenList tokens = createTokenList(&Tokens[0], nTokens);
    const int nRows = 10;
    const int nColoms = 33;
    char str[] = "puts(\"\\\"Hello\\\"\t\\n\");\nint ch = (int)32;\nlong g = -5;\ng = -g + ch * 10;\nreturn 3;";
    char nKeywords[nRows][nColoms];
    char nIdentifiers[nRows][nColoms];
    char nLiterals[nRows][nColoms];
    char nOperators[nRows][nColoms];
    char nDelimiters[nRows][nColoms];
    struct Table keywords = createTable(&nKeywords[0][0], nRows, nColoms);
    struct Table identifiers = createTable(&nIdentifiers[0][0], nRows, nColoms);
    struct Table literals = createTable(&nLiterals[0][0], nRows, nColoms);
    struct Table operators = createTable(&nOperators[0][0], nRows, nColoms);
    struct Table delimiters = createTable(&nDelimiters[0][0], nRows, nColoms);
    char *src = &str[0];
    puts("Code :");
    puts(str);
    printf("\n");
    parseCode(src, &keywords, &identifiers, &literals, &operators, &delimiters, &tokens);
    puts("Tokens :");
    printToken(&tokens);
    return 0;
}
//
struct Table createTable(char *items, int capacity, int rowCapacity)
{
    struct Table table;
    table.items = items;
    table.capacity = capacity;
    table.counter = 0;
    table.rowCapacity = rowCapacity;
    return table;
}
//
struct TokenList createTokenList(struct Token *items, int cap)
{
    struct TokenList list;
    list.items = items;
    list.capacity = cap;
    list.counter = 0;
    return list;
}
//
void addToken(struct TokenList *plist, struct Token newToken)
{
    int presentcounter = (*plist).counter;
    int index = presentcounter;
    (*plist).items[index] = newToken;
    int newcounter = presentcounter + 1;
    (*plist).counter = newcounter;
}
//
void addString(struct Table *ptable, const char *str)
{
    int rowIndex = (*ptable).counter;
    char *src = getStringAt(ptable, rowIndex);
    strcpy(src, str);
    (*ptable).counter += 1;
}
//
char *getStringAt(const struct Table *ptable, int index)
{
    char *src = (*ptable).items;
    src += index * (*ptable).rowCapacity;
    return src;
}
//
void printToken(struct TokenList *plist)
{
    for (int i = 0; i < (*plist).counter; i++)
    {
        if ((*plist).items[i].type == TOKEN_KEYWORD)
        {
            printf("TOKEN_KEYWORD\t\t");
            if ((*plist).items[i].sub_type == KW_INT)
            {
                printf("KW_INT\t\t");
            }
            else if ((*plist).items[i].sub_type == KW_LONG)
            {
                printf("KW_LONG\t\t");
            }
            else if ((*plist).items[i].sub_type == KW_RETURN)
            {
                printf("KW_RETURN\t");
            }
            printf("%s\n", (*plist).items[i].lexeme);
        }
        else if ((*plist).items[i].type == TOKEN_IDENTIFIER)
        {
            printf("TOKEN_IDENTIFIER\t\t\t");
            printf("%s\n", (*plist).items[i].lexeme);
        }
        else if ((*plist).items[i].type == TOKEN_LITERAL)
        {
            printf("TOKEN_LITERAL\t\t");
            if ((*plist).items[i].sub_type == LIT_INTEGER)
            {
                printf("LIT_INTEGER\t");
            }
            else if ((*plist).items[i].sub_type == LIT_FLOAT)
            {
                printf("LIT_FLOAT\t");
            }
            else if ((*plist).items[i].sub_type == LIT_STRING)
            {
                printf("LIT_STRING\t");
            }
            printf("%s\n", (*plist).items[i].lexeme);
        }
        else if ((*plist).items[i].type == TOKEN_DELIMITER)
        {
            printf("TOKEN_DELIMITER\t\t");
            if ((*plist).items[i].sub_type == DEL_COMMA)
            {
                printf("DEL_COMMA\t");
            }
            else if ((*plist).items[i].sub_type == DEL_SEMICOLON)
            {
                printf("DEL_SEMICOLON\t");
            }
            else if ((*plist).items[i].sub_type == DEL_LEFTPAR)
            {
                printf("DEL_LEFTPAR\t");
            }
            else if ((*plist).items[i].sub_type == DEL_RIGHTPAR)
            {
                printf("DEL_RIGHTPAR\t");
            }
            else if ((*plist).items[i].sub_type == DEL_LEFTBRACKET)
            {
                printf("DEL_LEFTBRACKET\t");
            }
            else if ((*plist).items[i].sub_type == DEL_RIGHTBRACKET)
            {
                printf("DEL_RIGHTBRACKET\t");
            }
            printf("%s\n", (*plist).items[i].lexeme);
        }
        else if ((*plist).items[i].type == TOKEN_OPERATOR)
        {
            printf("TOKEN_OPERATOR\t\t");
            if ((*plist).items[i].sub_type == OP_MULT)
            {
                printf("OP_MULT\t\t");
            }
            else if ((*plist).items[i].sub_type == OP_PLUS)
            {
                printf("OP_PLUS\t\t");
            }
            else if ((*plist).items[i].sub_type == OP_ASSIGNMENT)
            {
                printf("OP_ASSIGNMENT\t");
            }
            printf("%s\n", (*plist).items[i].lexeme);
        }
        else
        {
            printf("List is empty");
        }
    }
}
//
void parseCode(char *src, struct Table *keywords, struct Table *identifiers, struct Table *literals, struct Table *operators, struct Table *delimiters, struct TokenList *plist)
{
    const int bufLen = 10;
    char buf[bufLen];
    int Dot = 0;
    struct Token newToken;
    while (*src != '\0')
    {
        if (isspace(*src))
        {
            src++;
        }
        else if (isalpha(*src) || *src == '_')
        {
            src = readWord(src, buf, bufLen);
            if (src)
            {
                if (strcmp(buf, "int") == 0)
                {
                    addString(keywords, buf);
                    newToken.lexeme = getStringAt(keywords, (*keywords).counter - 1);
                    newToken.type = TOKEN_KEYWORD;
                    newToken.sub_type = KW_INT;
                    addToken(plist, newToken);
                }
                else if (strcmp(buf, "long") == 0)
                {
                    addString(keywords, buf);
                    newToken.lexeme = getStringAt(keywords, (*keywords).counter - 1);
                    newToken.type = TOKEN_KEYWORD;
                    newToken.sub_type = KW_LONG;
                    addToken(plist, newToken);
                }
				// else if (strcmp(buf, "return") == 0)
                // {
                //     addString(keywords, buf);
                //     newToken.lexeme = getStringAt(keywords, (*keywords).counter - 1);
                //     newToken.type = TOKEN_KEYWORD;
                //     newToken.sub_type = KW_RETURN;
                //     addToken(plist, newToken);
                // }
                else
                {
                    addString(identifiers, buf);
                    newToken.lexeme = getStringAt(identifiers, (*identifiers).counter - 1);
                    newToken.type = TOKEN_IDENTIFIER;
                    addToken(plist, newToken);
                }
            }
        }
        else if (isdigit(*src))
        {
            src = readNumber(src, buf, bufLen, &Dot);
            if (src)
            {
                if (Dot == 1)
                {
                    addString(literals, buf);
                    newToken.lexeme = getStringAt(literals, (*literals).counter - 1);
                    newToken.type = TOKEN_LITERAL;
                    newToken.sub_type = LIT_FLOAT;
                    addToken(plist, newToken);
                }
                else
                {
                    addString(literals, buf);
                    newToken.lexeme = getStringAt(literals, (*literals).counter - 1);
                    newToken.type = TOKEN_LITERAL;
                    newToken.sub_type = LIT_INTEGER;
                    addToken(plist, newToken);
                }
            }
        }
        else if (*src == '\"')
        {
            src = readString(src, buf, bufLen);
            if (src)
            {
                addString(literals, buf);
                newToken.lexeme = getStringAt(literals, (*literals).counter - 1);
                newToken.type = TOKEN_LITERAL;
                newToken.sub_type = LIT_STRING;
                addToken(plist, newToken);
            }
        }
        else if (isoperator(*src, &newToken))
        {
            if (bufLen < 2)
            {
                src = NULL;
            }
            else
            {
                buf[0] = *src;
                buf[1] = '\0';
                addString(operators, buf);
                newToken.lexeme = getStringAt(operators, (*operators).counter - 1);
                newToken.type = TOKEN_OPERATOR;
                addToken(plist, newToken);
                src++;
            }
        }
        else if (isdelimiter(*src, &newToken))
        {
            if (bufLen < 2)
            {
                src = NULL;
            }
            else
            {
                buf[0] = *src;
                buf[1] = '\0';
                addString(delimiters, buf);
                newToken.lexeme = getStringAt(delimiters, (*delimiters).counter - 1);
                newToken.type = TOKEN_DELIMITER;
                addToken(plist, newToken);
                src++;
            }
        }
        else
        {
            src ++;
        }
        if (src == NULL)
        {
            break;
        }
    }
}
//
char *readWord(char *str, char *dest, const int destLen)
{
    char *src = str;
    int counter = 0;
    while (isalnum(*src) || *src == '_')
    {
        *dest = *src;
        dest++;
        counter++;
        if (counter >= destLen)
        {
            return NULL;
        }
        src++;
    }
    *dest = '\0';
    return src;
}
//
char *readNumber(char *str, char *dest, const int destLen, int *Dot)
{
    char *src = str;
    int counter = 0;
    *Dot = 0;
    while (isdigit(*src))
    {
        *dest = *src;
        dest++;
        counter++;
        if (counter >= destLen)
        {
            return NULL;
        }
        src++;
    }
    if (*src == '.')
    {
        *Dot = 1;
        *dest = *src;
        dest++;
        counter++;
        if (counter >= destLen)
        {
            return NULL;
        }
        src++;
        if (!isdigit(*src))
        {
            return NULL;
        }
        else
        {
            while (isdigit(*src))
            {
                *dest = *src;
                dest++;
                counter++;
                if (counter >= destLen)
                {
                    return NULL;
                }
                src++;
            }
        }
    }
    *dest = '\0';
    return src;
}
//
char *readString(char *str, char *dest, const int destLen)
{
    char *src = str;
    int counter = 0;
    *dest = *src;
    dest++;
    src++;
    counter++;
    while (*src != '\"')
    {
        if (isgraph(*src) || isspace(*src) || isescape(*src))
        {
            *dest = *src;
            dest++;
            counter++;
            if (counter >= destLen)
            {
                return NULL;
            }
            src++;
        }
        else
        {
            return NULL;
        }
    }
    *dest = *src;
    dest++;
    src++;
    *dest = '\0';
    return src;
}
//
bool isescape(char ch)
{
    if (ch == '\b');
    else if (ch == '\f');
    else if (ch == '\n');
    else if (ch == '\r');
    else if (ch == '\t');
    else if (ch == '\v');
    else if (ch == '\\');
    else if (ch == '\'');
    else if (ch == '\"');
    else if (ch == '\?');
    else if (ch == '\0');
    else
    {
        return 0;
    }
    return 1;
}
//
bool isoperator(char ch, struct Token *token)
{
    if (ch == '+')
    {
        	(*token).sub_type = OP_PLUS;
    }
    else if (ch == '*')
    {
        	(*token).sub_type = OP_MULT;
    }
    else if (ch == '=')
    {
        	(*token).sub_type = OP_ASSIGNMENT;
    }
    else
    {
        return 0;
    }
    return 1;
}
//
bool isdelimiter(char ch, struct Token *token)
{
    if (ch == ';')
    {
        	(*token).sub_type = DEL_SEMICOLON;
    }
    else if (ch == ',')
    {
        	(*token).sub_type = DEL_COMMA;
    }
    else if (ch == '(')
    {
        	(*token).sub_type = DEL_LEFTPAR;
    }
    else if (ch == ')')
    {
        	(*token).sub_type = DEL_RIGHTPAR;
    }
    else if (ch == '{')
    {
        	(*token).sub_type = DEL_LEFTBRACKET;
    }
    else if (ch == '}')
    {
        	(*token).sub_type = DEL_RIGHTBRACKET;
    }
    else
    {
        return 0;
    }
    return 1;
}
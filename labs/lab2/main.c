// gcc main.c -lprogbase -lm
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <progbase.h> 

int main() {
    const float xmin = -10;
    const float xmax = 10;
    const float xstep = 0.5;
    float y;
    float x = xmin;
    while(x<=xmax)
    {
        if(x!=0){
            if(x>=-4.5 && x<=4.5)
            {
                if(x>=-1 && x<=1)
                {
                     y= 1 / (3 * x);
                    printf("y(%f) = ", x);
                    printFloat(y);
                    puts("");
                }
                else 
                {
                    y = sin (pow(x, 2)) + cos (pow((x),2)) / x;
                    printf("y(%f) = ", x);
                    printFloat(y);
                    puts(""); 
                }
            }
            else 
            {
                y = 1 / (3 * x);
                printf("y(%f) = ", x);
                printFloat(y);
                puts("");
            }
        }
        else 
        {
            printf("y(%f) = Error\n", x);
        }
        x += xstep;
    }
    return 0;
}
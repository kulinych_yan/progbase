#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <progbase.h> 
#include <time.h>
#include <progbase/console.h>
#include <progbase/canvas.h>


struct vec_2d
{
    int x;
    int y;
};

struct color
{
    int red;
    int green;
    int blue;
};

struct balls
{
    struct vec_2d loc;
    struct vec_2d vel;
    int r;
    struct color fill;
    struct color contur;
};

int random_org(int x, int y);


int main(void) {
    srand (time(0));
    //Canvas_invertYOrientation();
    struct ConsoleSize consoleSize = Console_size();
    const int width_pixels = consoleSize.columns;
    const int height_pixels = consoleSize.rows * 2;

    Canvas_setSize(width_pixels, height_pixels);

    int n;
    printf("Enter the number of balls = ");
    scanf("%d", &n);
    struct balls balls[n];

    for(int i = 0; i < n; i++)
    {
        balls[i].r = random_org(2, 10);
        balls[i].loc.x = random_org(balls[i].r, width_pixels - balls[i].r);
        balls[i].loc.y = random_org(balls[i].r, height_pixels - balls[i].r);
        balls[i].vel.x = random_org(-4, 4);
        balls[i].vel.y = random_org(-4, 4);
        balls[i].fill.red = random_org(0, 255);
        balls[i].fill.green = random_org(0, 255);
        balls[i].fill.blue = random_org(0, 255);
        balls[i].contur.red = random_org(0, 255);
        balls[i].contur.green = random_org(0, 255);
        balls[i].contur.blue = random_org(0, 255);
    }   
    Console_clear();

    const int OneFrameTime = 15;

    while(1)
    {
        time_t startTime = clock();
        float dt = OneFrameTime / 1000.0;
        //star Draw
        Canvas_beginDraw();
        //
        for(int i = 0; i < n; i++)
        {
            Canvas_setColorRGB(balls[i].fill.red, balls[i].fill.green, balls[i].fill.blue);
            Canvas_fillCircle(balls[i].loc.x, balls[i].loc.y, balls[i].r);
            Canvas_setColorRGB(balls[i].contur.red, balls[i].contur.green, balls[i].contur.blue);
            Canvas_strokeCircle(balls[i].loc.x, balls[i].loc.y, balls[i].r);
        }
        //
        Canvas_setColorRGB(255,255,255);
        for(int i = 1; i < n; i++)
        {
            Canvas_strokeLine(balls[0].loc.x, balls[0].loc.y, balls[i].loc.x, balls[i].loc.y);
        }
        //
        Canvas_endDraw();
        //end  Draw
        Console_setCursorAttribute(BG_CYAN);
        

        for(int i = 0; i < n; i++)
        {
            balls[i].loc.x += balls[i].vel.x;
            balls[i].loc.y += balls[i].vel.y;
        }
        for(int i = 0; i < n; i++)
        {
            if(balls[i].loc.x < balls[i].r || balls[i].loc.x > width_pixels - balls[i].r)
            {
                balls[i].vel.x = -balls[i].vel.x;
            }
            if(balls[i].loc.y < balls[i].r || balls[i].loc.y > height_pixels - balls[i].r)
            {
                balls[i].vel.y = - balls[i].vel.y;
            }
        }

        time_t endTime = clock();
        time_t elapsedTime = endTime - startTime;
        float millis = elapsedTime / (float)CLOCKS_PER_SEC;
        int sleepTime = millis;
        if(millis < OneFrameTime)
        {
            sleepTime = OneFrameTime;
            sleepMillis(sleepTime);
        }
    }
   return 0;
}

int random_org(int x, int y)
{
    int r;
    r = rand() % (y - x + 1) + x;
    return r;
}
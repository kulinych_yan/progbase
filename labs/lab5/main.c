#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <progbase.h> 
#include <progbase/console.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>

int conReadLine(char str[], int maxBufLen);

int main(void) {
    bool main_menu = true;
    bool chQuit;
    bool strQuit;

    char key;
    char chSw;
    char strSw;

    int rmin = 32;
    int rmax = 128;

    int i = 0;
    int j = 0;
    int buflen = 0;

    while(main_menu == true)
    {
        Console_clear();
        chQuit = true;
        strQuit = true;
        printf("1.Characters\n");
        printf("2.String\n");
        printf("0.Quit\n");
        key = Console_getChar();
        switch(key)
        {
            case '1':
                Console_clear();
                while(chQuit == true)
                {
                    printf("1. Alphanumeric\n");
                    printf("2. Alphabetic (lowercase)\n");
                    printf("3. Alphabetic (uppercase)\n");
                    printf("4. Alphabetic (all)\n");
                    printf("5. Decimal digit\n");
                    printf("6. Hexadecimal digit\n");
                    printf("7. Punctuation\n");
                    printf("0. < Back\n");
                    puts("");
                    chSw = Console_getChar();
                    Console_clear();
                    puts("");
                    switch(chSw)
                    {
                        case '1':
                            for(i = 0 ; i < 128; i ++)
                            {
                                if(isalnum(i))
                                printf("%c ", i);
                            }
                            puts("");
                            break;
                        case '2':
                            for(i = 0; i < 128; i ++)
                            {
                                if(islower(i))
                                printf("%c ", i);
                            }
                            puts("");
                            break;
                        case '3':
                            for(i = 0; i < 128; i ++)
                            {
                                if(isupper(i))
                                printf("%c ", i);
                            }
                            puts("");
                            break;
                        case '4':
                            for(i = 0; i < 128; i ++)
                            {
                                if(isalpha(i))
                                printf("%c ", i);
                            }
                            puts("");
                            break;
                        case '5':
                            for(i = 0; i < 128; i ++)
                            {
                                if(isdigit(i))
                                printf("%c ", i);
                            }
                            puts("");
                            break;
                        case '6':
                            for(i = 0; i < 128; i ++)
                            {
                                if(isxdigit(i))
                                printf("%c ", i);
                            }
                            puts("");
                            break;
                        case '7':
                            for(i = 0; i < 128; i ++)
                            {
                                if(ispunct(i))
                                printf("%c ", i);
                            }
                            puts("");
                            break;
                        case '0':
                        {
                            chQuit = false;
                            break;
                        }
                    }
                }
                break;
            case '2':
            {
                Console_clear();
                int N = 0;
                printf("Enter N: ");
                scanf("%i", &N);
                char buf[N];
                for (int i = 0; i < N; i++)
                {
                    buf[i] = rand() % (rmax - rmin + 1) + rmin;
                }
                buf[N - 1] = '\0';
                int buflen = strlen(buf);
                while (strQuit == true)
                {
                    Console_clear();
                    printf("1. Replace the row in the array with the entered string value from the console.\n");
                    printf("2. Clear line (UPD to make it empty, line length must be 0).\n");
                    printf("3. Output a substring from a given position and a given length.\n");
                    printf("4. Output a list of substrings separated by a given character.\n");
                    printf("5. Display the shortest word.\n");
                    printf("6. Find and print all the fractional numbers contained in a line.\n");
                    printf("7. Find and output a product of all integers contained in a line.\n");
                    printf("0. < Back\n");
                    printf("\nString (%i):\n", buflen);
                    Console_setCursorAttribute(BG_RED);
                    printf("%s\n", buf);
                    Console_reset();
                    key = Console_getChar();
                    Console_clear();
                    switch (key)
                    {
                    case '1':
                    {
                        printf("Enter your string: \n");
                        buflen = conReadLine(buf, N);
                        break;
                    }
                    case '2':
                    {
                        buflen = 0;
                        buf[0] = '\0';
                        break;
                    }
                    case '3':
                    {
                        int P = 0;
                        int L = 0;
                        char tmp[N];
                        printf("Start pos: ");
                        scanf("%i", &P);
                        if (P >= N - 1)
                        {
                            Console_clear();
                            printf("Enter a correct position");
                        }
                        else
                        {
                            Console_clear();
                            printf("Length: ");
                            scanf("%i", &L);
                            Console_clear();
                            int i = 0;
                            int j = P;
                            do
                            {
                                tmp[i] = buf[j];
                                i++;
                                j++;
                            } while (j <= L + P);
                            tmp[L + P - 1] = '\0';
                            int tmplen = strlen(tmp);
                            printf("String (%i):\n", tmplen);
                            Console_setCursorAttribute(BG_RED);
                            printf("%s", tmp);
                            Console_reset();
                        }
                        printf("\n\nPress any key to procceed back");
                        Console_getChar();
                        break;
                    }
                    case '4':
                    {
                        char tmp[N];
                        char sym = 0;
                        int check = 0;
                        printf("Choose a symbol: ");
                        scanf("%c", &sym);
                        Console_clear();
                        for (int i = 0; i < N; i++)
                        {
                            if (buf[i] == sym && buf[i + 1] != '\0')
                            {
                                tmp[i] = '\n';
                                check++;
                            }
                            else
                            {
                                tmp[i] = buf[i];
                            }
                        }
                        if (check == 0)
                        {
                            printf("\nNo such symbol found");
                        }
                        else
                        {
                            printf("%s\n", tmp);
                            printf("\n\nPress any key to procceed back");
                        }
                        Console_getChar();
                        break;
                    }
                    case '5':
                    {
                        int h = 0, l = -1, max = 900, min = -1;
                        for (int i = 0; buf[i] != '\0'; i++)
                            if (isalpha(buf[i]))
                            {
                                if (l == -1)
                                {
                                    l = i;
                                }
                                h++;
                            }
                            else
                            {
                                if (h != 0 && h < max)
                                {
                                    max = h;
                                    min = l;
                                }
                                l = -1;
                                h = 0;
                            }
                        if (min == -1)
                        {
                            printf("No words in the string\n");
                            printf("\nPress any key to procceed back");
                            Console_getChar();
                            break;
                        }
                        printf("The shortest word is ");
                        while (isalpha(buf[min]))
                        {
                            printf("%c", buf[min++]);
                        }
                        printf("'");
                        puts("");
                        printf("\n\nPress any key to procceed back");
                        Console_getChar();
                        break;
                    }
                    case '6':
                    {
                        char *pEnd;
                        double d = strtod(buf, &pEnd);
                        while (d)
                        {
                            printf("%lf ", d);
                            d = strtod(pEnd, &pEnd);
                        }
                        printf("\n\nPress any key to procceed back");
                        Console_getChar();
                        break;
                    }
                    case '7':
                    {
                        int f = 1;
                        for (int i = 0; i < buflen; i++)
                        {
                            if (isdigit(buf[i]))
                            {
                                f = (buf[i]-'0') * f;
                            }
                        }
                        printf("The product is: %i", f);
                        printf("\n\nPress any key to procceed back");
                        Console_getChar();
                        break;
                    }
                    case '0':
                    {
                        strQuit = false;
                        break;
                    }
                    default:
                    {
                        Console_clear();
                        break;
                    }
                    }
                }
                break;
            }
            break;
            case '0':
            {
                main_menu = false;
                break;
            }
            default: 
            {
                break;
            }
        }
    }
   return 0;
}

int conReadLine(char str[], int maxBufLen)
{
    for (char ch; (ch = getchar()) != '\n';)
    {
    }
    fgets(str, maxBufLen, stdin);
    int bufLength = strlen(str);
    if (str[bufLength - 1] == '\n')
    {
        str[bufLength - 1] = '\0';
        bufLength -= 1;
    }
    else
    {
        for (char ch; (ch = getchar()) != '\n';)
        {
        }
    }
    return bufLength;
}
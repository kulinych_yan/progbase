// ���������� �� ���������:
// gcc main.c -lprogbase -lm

#include <stdio.h>    // ��� ����� � �������
#include <math.h>     // ��� ������������ �������
#include <stdlib.h>   // ���� �������� �������
#include <progbase.h> // ��������� ��� � ���� ����� � �������
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <progbase/console.h>

enum TokenType
{
    TOKEN_WHITESPACE,
    TOKEN_KEYWORD,
    TOKEN_IDENTIFIER,
    TOKEN_LITERAL,
    TOKEN_OPERATOR,
    TOKEN_DELIMITER,
};
enum TokenKeywords
{
   KW_INT,
   KW_IF,
   KW_RETURN,
};
enum TokenLiterals
{
   LIT_INTEGER,
   LIT_FLOAT,
   LIT_STRING,
};
enum TokenOperators
{
   OP_PLUS,
   OP_MULT,
   OP_ASSIGNMENT,
   OP_LESS,
   OP_GREATER,
};
enum TokenDelimiters
{
   DEL_LEFTPAR,
   DEL_RIGHTPAR,
   DEL_LEFTCURVE,
   DEL_RIGHTCURVE,
   DEL_COMMA,
   DEL_SEMICOLON,
};

struct StringTable
{
   char * items;
   size_t capacity;
   size_t count;
   size_t rowCapacity;
};
struct TokenList
{
    struct Token * items;
    size_t capacity;
    size_t count;
};
struct TextPosition 
{ 
    int row, column; 
};
struct Token
{
   char * lexeme;
   enum TokenType type;
   int    subType;
   struct TextPosition position;
};

struct StringTable createStringTable(char *items, int cap, int rowCap);
struct TokenList createTokenList(struct Token * a, int n);
void addToken(struct TokenList * plist, struct Token newToken);
void addString(struct StringTable *ptable, const char *str);
char * getStringAt(const struct StringTable *ptable, int index);
void printToken(struct TokenList * plist, FILE * p);
void parseCode(FILE * p, char * ch, struct StringTable *whitespaces, struct StringTable *keywords, struct StringTable * identifiers, struct StringTable *literals, struct StringTable *operators, struct StringTable *delimiters, struct TokenList * plist);
FILE * readWord(FILE * p, char * ch, char * dest, const int destSize);
FILE * readNumber(FILE * p, char * ch, char * dest, const int destSize, int * Dot);
FILE * readString(FILE * p, char * ch, char * dest, const int destSize);
bool isescape(char ch);
bool isoperator(char ch, struct Token * token);
bool isdelimiter(char ch, struct Token * token);

int main(int argc, char *argv[argc]) {
    // ������� ��������
    const int nTokens = 100;
    struct Token Tokens[nTokens];
    struct TokenList tokens = createTokenList(&Tokens[0], nTokens);

    const int nRows = 20;
    const int nColoms = 33;
    char nWhitespaces[nRows][nColoms];
    char nKeywords[nRows][nColoms];
    char nIdentifiers[nRows][nColoms];
    char nLiterals[nRows][nColoms];
    char nOperators[nRows][nColoms];
    char nDelimiters[nRows][nColoms];
    struct StringTable whitespaces = createStringTable(&nWhitespaces[0][0], nRows, nColoms);
    struct StringTable keywords = createStringTable(&nKeywords[0][0], nRows, nColoms);
    struct StringTable identifiers = createStringTable(&nIdentifiers[0][0], nRows, nColoms);
    struct StringTable literals = createStringTable(&nLiterals[0][0], nRows, nColoms);
    struct StringTable operators = createStringTable(&nOperators[0][0], nRows, nColoms);
    struct StringTable delimiters = createStringTable(&nDelimiters[0][0], nRows, nColoms);
    FILE * string;
    FILE * out = stdout;
    bool outfile = false;
    bool open = false;
    if(argc > 1)
    {
        for(int i = 1; i < argc; i++)
        {
            if(strcmp(argv[i], "-o") == 0)
            {
                if(i == argc - 1)
                {
                    return 0;
                }
                else
                {
                    outfile = true;
                }
            }
            else if(isgraph(*argv[i]) && *argv[i] != '-')
            {
                if(outfile)
                {
                    out = fopen(argv[i], "w");
                    open = true;
                    outfile = false;
                }
                else
                {
                    string = fopen(argv[i], "r");
                }
            }
            else if(outfile)
            {
                return 0;
            }
            else if(strcmp(argv[i], "-1") == 0)
            {
                return 0;
            }
            else
            {
                return 0;
            }
        }
    }
    else
    {
        string = fopen("input.txt", "r");
        out = stdout;
    }
    if(string == NULL || (outfile && out == NULL))
    {
        puts("Cannot open file");
    }
    else
    {
        char ch = 0;
        fprintf(out,"Code >>>\n");
        ch = fgetc(string);
        while(ch != EOF) {
            fputc(ch, out);
            ch = fgetc(string);
        }
        //fseek(string, 0, SEEK_SET);
        rewind(string);
        fprintf(out,"<<<\n\n");
        ch = fgetc(string);
        parseCode(string, &ch, &whitespaces, &keywords, &identifiers, &literals, &operators, &delimiters, &tokens);
        fprintf(out,"Tokens >>>\n");
        printToken(&tokens, out);
        fprintf(out,"<<<\n");
    }
    fclose(string);
    if(open)
    {
        fclose(out);
    }
    // ʳ���� ��������
    return 0;
}

struct StringTable createStringTable(char *items, int cap, int rowCap)
{
   struct StringTable table;
   table.items = items;
   table.capacity = cap;
   table.count = 0;
   table.rowCapacity = rowCap;
   return table; 
}
struct TokenList createTokenList(struct Token * items, int cap)
{
    struct TokenList list;
    list.items = items;
    list.capacity = cap;
    list.count = 0;
    return list;
}
void addToken(struct TokenList * plist, struct Token newToken)
{
    int presentCount = (*plist).count;
    int index = presentCount;
    (*plist).items[index] = newToken;
    int newCount = presentCount + 1;
    (*plist).count = newCount;
}
void addString(struct StringTable *ptable, const char *str)
{
    int rowIndex = (*ptable).count;
    char *p = getStringAt(ptable, rowIndex);
    strcpy(p, str);
    (*ptable).count += 1;
}
char * getStringAt(const struct StringTable *ptable, int index)
{
   char *p = (*ptable).items;
   p += index * (*ptable).rowCapacity;
   return p;
}
void printToken(struct TokenList * plist, FILE * p)
{
    for(int i = 0; i < (*plist).count; i++)
    {
        if((*plist).items[i].type == TOKEN_KEYWORD)
        {
            fprintf(p,"TOKEN_KEYWORD\t\t");
            if((*plist).items[i].subType == KW_INT)
            {
                fprintf(p,"KW_INT\t\t");
            }
            else if((*plist).items[i].subType == KW_IF)
            {
                fprintf(p,"KW_IF\t\t");
            }
            else if((*plist).items[i].subType == KW_RETURN)
            {
                fprintf(p,"KW_RETURN\t");
            }
            fprintf(p,"R: %i\t", (*plist).items[i].position.row);
            fprintf(p,"C: %i\t", (*plist).items[i].position.column);
            fprintf(p,"\"%s\"\n", (*plist).items[i].lexeme);
        }
        else if((*plist).items[i].type == TOKEN_IDENTIFIER)
        {
            fprintf(p,"TOKEN_IDENTIFIER\t\t\t");
            fprintf(p,"R: %i\t", (*plist).items[i].position.row);
            fprintf(p,"C: %i\t", (*plist).items[i].position.column);
            fprintf(p,"\"%s\"\n", (*plist).items[i].lexeme);
        }
        else if((*plist).items[i].type == TOKEN_LITERAL)
        {
            fprintf(p,"TOKEN_LITERAL\t\t");
            if((*plist).items[i].subType == LIT_INTEGER)
            {
                fprintf(p,"LIT_INTEGER\t");
            }
            else if((*plist).items[i].subType == LIT_FLOAT)
            {
                fprintf(p,"LIT_FLOAT\t");
            }
            else if((*plist).items[i].subType == LIT_STRING)
            {
                fprintf(p,"LIT_STRING\t");
            }
            fprintf(p,"R: %i\t", (*plist).items[i].position.row);
            fprintf(p,"C: %i\t", (*plist).items[i].position.column);
            fprintf(p,"\"%s\"\n", (*plist).items[i].lexeme);
        }
        else if((*plist).items[i].type == TOKEN_DELIMITER)
        {
            fprintf(p,"TOKEN_DELIMITER\t\t");
            if((*plist).items[i].subType == DEL_COMMA)
            {
                fprintf(p,"DEL_COMMA\t");
            }
            else if((*plist).items[i].subType == DEL_SEMICOLON)
            {
                fprintf(p,"DEL_SEMICOLON\t");
            }
            else if((*plist).items[i].subType == DEL_LEFTPAR)
            {
                fprintf(p,"DEL_LEFTPAR\t");
            }
            else if((*plist).items[i].subType == DEL_RIGHTPAR)
            {
                fprintf(p,"DEL_RIGHTPAR\t");
            }
            else if((*plist).items[i].subType == DEL_LEFTCURVE)
            {
                fprintf(p,"DEL_LEFTCURVE\t");
            }
            else if((*plist).items[i].subType == DEL_RIGHTCURVE)
            {
                fprintf(p,"DEL_RIGHTCURVE\t");
            }
            fprintf(p,"R: %i\t", (*plist).items[i].position.row);
            fprintf(p,"C: %i\t", (*plist).items[i].position.column);
            fprintf(p,"\"%s\"\n", (*plist).items[i].lexeme);
        }
        else if((*plist).items[i].type == TOKEN_OPERATOR)
        {
            fprintf(p,"TOKEN_OPERATOR\t\t");
            if((*plist).items[i].subType == OP_MULT)
            {
                fprintf(p,"OP_MULT\t\t");
            }
            else if((*plist).items[i].subType == OP_PLUS)
            {
                fprintf(p,"OP_PLUS\t\t");
            }
            else if((*plist).items[i].subType == OP_ASSIGNMENT)
            {
                fprintf(p,"OP_ASSIGNMENT\t");
            }
            else if((*plist).items[i].subType == OP_LESS)
            {
                fprintf(p,"OP_LESS\t\t");
            }
            else if((*plist).items[i].subType == OP_GREATER)
            {
                fprintf(p,"OP_GREATER\t");
            }
            fprintf(p,"R: %i\t", (*plist).items[i].position.row);
            fprintf(p,"C: %i\t", (*plist).items[i].position.column);
            fprintf(p,"\"%s\"\n", (*plist).items[i].lexeme);
        }
        else if((*plist).items[i].type == TOKEN_WHITESPACE)
        {
            fprintf(p,"TOKEN_WHITESPACE\t\t\t");
            fprintf(p,"R: %i\t", (*plist).items[i].position.row);
            fprintf(p,"C: %i\t", (*plist).items[i].position.column);
            fprintf(p,"\"%s\"\n", (*plist).items[i].lexeme);
        }
        else
        {
            fprintf(p,"List is empty\n");
        }
    }
}
void parseCode(FILE * p, char * ch, struct StringTable *whitespaces, struct StringTable *keywords, struct StringTable * identifiers, struct StringTable *literals, struct StringTable *operators, struct StringTable *delimiters, struct TokenList * plist)
{
    const int bufSize = 10;
    char buf[bufSize];
    int Dot = 0;
    int RowPosition = 1;
    int ColumnPosition = 0;
    struct Token newToken;
    while(*ch != EOF)
    {
        if(isspace(*ch))
        {
            if(bufSize < 2)
            {
                p = NULL;
            }
            else
            {
                buf[0] = *ch;
                buf[1] = '\0';
                addString(whitespaces, buf);
                newToken.lexeme = getStringAt(whitespaces, (*whitespaces).count - 1);
                newToken.type = TOKEN_WHITESPACE;
                newToken.position.column = ftell(p) - ColumnPosition;
                newToken.position.row = RowPosition;
                addToken(plist, newToken);
                *ch = fgetc(p);
                if(*ch == '\n')
                {
                    ColumnPosition = ftell(p);
                    RowPosition++;
                }
            }
        }
        else if(isalpha(*ch) || *ch == '_')
        {
            newToken.position.column = ftell(p) - ColumnPosition;
            p = readWord(p, ch, buf, bufSize);
            if(p != NULL)
            {
                if(strcmp(buf, "int") == 0)
                {
                    addString(keywords, buf);
                    newToken.lexeme = getStringAt(keywords, (*keywords).count - 1);
                    newToken.type = TOKEN_KEYWORD;
                    newToken.subType = KW_INT;
                    newToken.position.row = RowPosition;
                    addToken(plist, newToken);
                }
                else if(strcmp(buf, "if") == 0)
                {
                    addString(keywords, buf);
                    newToken.lexeme = getStringAt(keywords, (*keywords).count - 1);
                    newToken.type = TOKEN_KEYWORD;
                    newToken.subType = KW_IF;
                    newToken.position.row = RowPosition;
                    addToken(plist, newToken);
                }
                else
                {
                    addString(identifiers, buf);
                    newToken.lexeme = getStringAt(identifiers, (*identifiers).count - 1);
                    newToken.type = TOKEN_IDENTIFIER;
                    newToken.position.row = RowPosition;
                    addToken(plist, newToken);
                }
            }
        }
        else if(isdigit(*ch))
        {
            newToken.position.column = ftell(p) - ColumnPosition;
            p = readNumber(p, ch, buf, bufSize, &Dot);
            if(p != NULL)
            {
                if(Dot == 1)
                {
                    addString(literals, buf);
                    newToken.lexeme = getStringAt(literals, (*literals).count - 1);
                    newToken.type = TOKEN_LITERAL;
                    newToken.subType = LIT_FLOAT;
                    newToken.position.row = RowPosition;
                    addToken(plist, newToken);
                }
                else
                {
                    addString(literals, buf);
                    newToken.lexeme = getStringAt(literals, (*literals).count - 1);
                    newToken.type = TOKEN_LITERAL;
                    newToken.subType = LIT_INTEGER;
                    newToken.position.row = RowPosition;
                    addToken(plist, newToken);
                }
            }
        }
        else if(*ch == '\"')
        {
            *ch = fgetc(p);
            newToken.position.column = ftell(p) - ColumnPosition;
            p = readString(p, ch, buf, bufSize);
            if(p != NULL)
            {
                addString(literals, buf);
                newToken.lexeme = getStringAt(literals, (*literals).count - 1);
                newToken.type = TOKEN_LITERAL;
                newToken.subType = LIT_STRING;
                newToken.position.row = RowPosition;
                addToken(plist, newToken);
            }
        }
        else if(isoperator(*ch, &newToken))
        {
            if(bufSize < 2)
            {
                p = NULL;
            }
            else
            {
                buf[0] = *ch;
                buf[1] = '\0';
                addString(operators, buf);
                newToken.lexeme = getStringAt(operators, (*operators).count - 1);
                newToken.type = TOKEN_OPERATOR;
                newToken.position.column = ftell(p) - ColumnPosition;
                newToken.position.row = RowPosition;
                addToken(plist, newToken);
                *ch = fgetc(p);
            }
        }
        else if(isdelimiter(*ch, &newToken))
        {
            if(bufSize < 2)
            {
                p = NULL;
            }
            else
            {
                buf[0] = *ch;
                buf[1] = '\0';
                addString(delimiters, buf);
                newToken.lexeme = getStringAt(delimiters, (*delimiters).count - 1);
                newToken.type = TOKEN_DELIMITER;
                newToken.position.column = ftell(p) - ColumnPosition;
                newToken.position.row = RowPosition;
                addToken(plist, newToken);
                *ch = fgetc(p);
            }
        }
        else
        {
            *ch = fgetc(p);
        }
        if(p == NULL)
        {
            fprintf(p,"Error:\n");
            break;
        }
    }
}
FILE * readWord(FILE * p, char * ch, char * dest, const int destSize)
{
    int count = 0;
    while(isalnum(*ch) || *ch == '_')
    {
        *dest = *ch;
        dest++;
        count++;
        if(count >= destSize)
        {
            return NULL;
        }
        *ch = fgetc(p);
    }
    *dest = '\0';
    return p;
}
FILE * readNumber(FILE * p, char * ch, char * dest, const int destSize, int * Dot)
{
    int count = 0;
    *Dot = 0;
    while(isdigit(*ch))
    {
        *dest = *ch;
        dest++;
        count++;
        if(count >= destSize)
        {
            return NULL;
        }
        *ch = fgetc(p);
    }
    if(*ch == '.')
    {
        *Dot = 1;
        *dest = *ch;
        dest++;
         count++;
        if(count >= destSize)
        {
            return NULL;
        }
        *ch = fgetc(p);
        if(!isdigit(*ch))
        {
            return NULL;
        }
        else
        {
            while(isdigit(*ch))
            {
                *dest = *ch;
                dest++;
                count++;
                if(count >= destSize)
                {
                    return NULL;
                }
                *ch = fgetc(p);
            }
        }
    }
    *dest = '\0';
    return p;
}
FILE * readString(FILE * p, char * ch, char * dest, const int destSize)
{
    int count = 0;
    bool Backslash = false;
    while(*ch != '\"')
    {
        if(isgraph(*ch) || isspace(*ch) || isescape(*ch))
        {
            *dest = *ch;
            dest++;
            count++;
            if(count >= destSize)
            {
                return NULL;
            }
            *ch = fgetc(p);
        }
        else
        {
            return NULL;
        }
    }
    *ch = fgetc(p);
    *dest = '\0';
    return p;
}
bool isescape(char ch)
{
    if(ch == '\b');
    else if(ch == '\f');
    else if(ch == '\n');
    else if(ch == '\r');
    else if(ch == '\t');
    else if(ch == '\v');
    else if(ch == '\\');
    else if(ch == '\'');
    else if(ch == '\"');
    else if(ch == '\?');
    else if(ch == '\0');
    else
    {
        return false;
    }
    return true;
}
bool isoperator(char ch, struct Token * token)
{
    if(ch == '+')
    {
        token->subType = OP_PLUS;
    }
    else if(ch == '*')
    {
        token->subType = OP_MULT;
    }
    else if(ch == '>')
    {
        token->subType = OP_GREATER;
    }
    else if(ch == '<')
    {
        token->subType = OP_LESS;
    }
    else if(ch == '=')
    {
        token->subType = OP_ASSIGNMENT;
    }
    else
    {
        return false;
    }
    return true;
}
bool isdelimiter(char ch, struct Token * token)
{
    if(ch == ';')
    {
        token->subType = DEL_SEMICOLON;
    }
    else if(ch == ',')
    {
        token->subType = DEL_COMMA;
    }
    else if(ch == '(')
    {
        token->subType = DEL_LEFTPAR;
    }
    else if(ch == ')')
    {
        token->subType = DEL_RIGHTPAR;
    }
    else if(ch == '{')
    {
        token->subType = DEL_LEFTCURVE;
    }
    else if(ch == '}')
    {
        token->subType = DEL_RIGHTCURVE;
    }
    else
    {
        return false;
    }
    return true;
}